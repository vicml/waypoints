class WaypointsController < ApplicationController
  before_action :set_waypoint, only: [:show, :edit, :update, :destroy]
  protect_from_forgery :except => [:api_v1_gps]
  # GET /waypoints
  # GET /waypoints.json
  def index
    @waypoints = Waypoint.all
  end

  # GET /waypoints/1
  # GET /waypoints/1.json
  def show
  end

  # GET /waypoints/new
  def new
    @waypoint = Waypoint.new
  end

  # GET /waypoints/1/edit
  def edit
  end

  # POST /waypoints
  # POST /waypoints.json
  def create
    @waypoint = Waypoint.new(waypoint_params)

    respond_to do |format|
      if @waypoint.save
        format.html { redirect_to @waypoint, notice: 'Waypoint was successfully created.' }
        format.json { render :show, status: :created, location: @waypoint }
      else
        format.html { render :new }
        format.json { render json: @waypoint.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /api/v1/gps
  # POST /api/v1/gps
  # {
  #   "latitude": 20.23,
  #   "longitude": -0.56,
  #   "sent_at": 2016-06-02 20:45:00",
  #   "vehicle_identifier": "HA-3452"
  # }
  def api_v1_gps
    @waypoint = Waypoint.new(
      {
        latitude: params[:latitude],
        longitude: params[:longitude], 
        sent_at: params[:sent_at], 
        vehicle_id: Vehicle.find_or_create_by({identifier: params[:vehicle_identifier]}).id
      }
    )

    ActionCable.server.broadcast "notifications", {html: "<div>Information updated at #{DateTime.now }</div>"}

    respond_to do |format|
      if @waypoint.save
        format.html { render :show, status: :created, location: @waypoint }
        format.json { render :show, status: :created, location: @waypoint }
      else
        format.html { render json: @waypoint.errors, status: :unprocessable_entity }
        format.json { render json: @waypoint.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /waypoints/1
  # PATCH/PUT /waypoints/1.json
  def update
    respond_to do |format|
      if @waypoint.update(waypoint_params)
        format.html { redirect_to @waypoint, notice: 'Waypoint was successfully updated.' }
        format.json { render :show, status: :ok, location: @waypoint }
      else
        format.html { render :edit }
        format.json { render json: @waypoint.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /waypoints/1
  # DELETE /waypoints/1.json
  def destroy
    @waypoint.destroy
    respond_to do |format|
      format.html { redirect_to waypoints_url, notice: 'Waypoint was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_waypoint
      @waypoint = Waypoint.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def waypoint_params
      params.require(:waypoint).permit(:latitude, :longitude, :sent_at, :vehicle_id)
    end
end
