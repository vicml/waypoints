class Vehicle < ApplicationRecord
	has_many :waypoints, dependent: :delete_all
	def last_waypoint
		self.waypoints.order(:sent_at).last
	end
end
