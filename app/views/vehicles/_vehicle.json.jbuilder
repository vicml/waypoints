json.extract! vehicle, :id, :identifier, :created_at, :updated_at
json.url vehicle_url(vehicle, format: :json)
