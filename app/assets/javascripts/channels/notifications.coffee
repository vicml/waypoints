App.notifications = App.cable.subscriptions.create "NotificationsChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->    
    $("#notice").html(data.html);
    $.ajax url: "/show", sync: false, dataType: 'script'
    
