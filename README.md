# README

* Ruby version 5.1.4


This app accept GPS waypoints associated to a vehicle, and then show them in a map. A GPS waypoint represents a location based on a latitude/longitude pair, and a point in time. A vehicle is represented by a unique alphanumeric identifier, and can have multiple GPS waypoints.

Specifically, the app consists of the following endpoints:

A JSON API endpoint named /api/v1/gps that accepts GPS waypoints associated to a vehicle. The following format must be used:
```    	
{
  "latitude": 20.23,
  "longitude": -0.56,
  "sent_at": "2016-06-02 20:45:00",
  "vehicle_identifier": "HA-3452"
}
```    	
If no vehicle exists with that identifier, create it.

An HTML endpoint named /show that shows the vehicles' most recent coordinate in a Google Maps.



* Deployment instructions
  * Install Ruby on Rails [Install Ruby on Rails](http://rubyonrails.org.es/instala.html).
  
  * Install Node.js
```    
sudo apt-get update
sudo apt-get install nodejs
```    
  * Install Redis [Install Redis](https://www.rosehosting.com/blog/how-to-install-configure-and-use-redis-on-ubuntu-16-04/).    
```
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install redis-server
```    
  * Clone Project
```
git clone https://bitbucket.org/vicml/waypoints.git
```
  * Install Gems
```	
bundle install
```    
  * Prepare database with example data
```    
rake db:create
rake db:migrate
rake db:seed
```    	

* Test Instructions
```    	
  rake test
```    	

* To see Demo go to:  http://52.42.202.66:3000/show

