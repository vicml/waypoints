class CreateWaypoints < ActiveRecord::Migration[5.1]
  def change
    create_table :waypoints do |t|
      t.string :latitude
      t.string :longitude
      t.datetime :sent_at
      t.references :vehicle, foreign_key: true

      t.timestamps
    end
  end
end
