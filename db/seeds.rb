# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
vehicle_list = [
  "HA-3452",
  "HA-3453",
  "HA-3462",
  "HA-3455"
]

vehicle_list.each do |identifier|
  Vehicle.create( identifier: identifier)
end


waypoint_list = [
  [ Vehicle.find_by_identifier("HA-3452").id,"20.23", "-0.56","2017-11-17 20:45:00"],  
  [ Vehicle.find_by_identifier("HA-3453").id,"20.24", "-0.59","2017-11-17 20:45:00"],  
  [ Vehicle.find_by_identifier("HA-3462").id,"20.24", "-0.57","2017-11-17 20:45:00"],  
  [ Vehicle.find_by_identifier("HA-3455").id,"20.24", "-0.58","2017-11-17 20:45:00"],    
]

waypoint_list.each do |vehicle_id, lat, lng, sent_at|
	Waypoint.create(vehicle_id: vehicle_id, latitude: lat, longitude:lng, sent_at: sent_at)	
end