Rails.application.routes.draw do
  resources :waypoints
  resources :vehicles
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'show', :to => 'vehicles#index'  
  post 'api/v1/gps', :to => 'waypoints#api_v1_gps'  
  get 'api/v1/gps', :to => 'waypoints#api_v1_gps'  
  root :to => redirect('/show')
end
